from names_generator import generate_name
import random

types = ['Sword', 'Axe', 'Bow', 'Crossbow', 'Dagger', 'Helm', 'Chest plate', 'Legging', 'Shoe']


class Effect:

    def __init__(self):
        pass


class Product:

    name: str
    id: int
    type: str
    effects: Effect

    def __init__(self):
        self.name = generate_name(style='capital')
        self.id = random.randint(1, 100000)
        self.type = types[random.randint(0, len(types) - 1)]

    def serialize(self):
        data = dict()
        for key in self.__dict__.keys():
            data[key] = self.__dict__[key]
        return data
